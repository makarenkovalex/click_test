<?php
namespace Makarenkov\ClickTest\Generator;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Exception;
use Makarenkov\ClickTest\Exception\FolderExistsException;
use Makarenkov\ClickTest\Path;

class Generator
{
    protected $mainExtension;
    protected $extensions = [
        'csv',
        'jpg',
        'txt',
        'doc',
        'png',
        'php',
        'json',
    ];

    protected $geoCodes = [
        'RU',
        'EN',
        'FR',
        'FI',
        'IT', # https://www.youtube.com/watch?v=FbqE9C-EavM
        'ZU',
    ];

    protected $dirMaxDeep;
    protected $dirMaxCount;
    protected $fileMaxLines;
    protected $fileMaxCount;

    public function __construct($mainExtension = 'csv')
    {
        $this->dirMaxDeep = getenv('GENERATOR_DIR_MAX_DEEP');
        $this->dirMaxCount = getenv('GENERATOR_DIR_MAX_COUNT');
        $this->fileMaxLines = getenv('GENERATOR_FILE_MAX_LINES');
        $this->fileMaxCount = getenv('GENERATOR_FILE_MAX_COUNT');
        $this->mainExtension = $mainExtension;
        if ($extPos = array_search($mainExtension, $this->extensions)) {
            unset($this->extensions[$extPos]);
        }
    }

    public function generate($folderName = 'output')
    {
        $fullPath = Path::base($folderName);
        $this->generateRecursive($fullPath, mt_rand(1, $this->dirMaxCount), $this->dirMaxDeep);
    }

    protected function generateRecursive($path, $foldersCount, $deepLevel)
    {
        // first check end of recursuion
        if ($deepLevel < 0) {
            return;
        } else {
            // if not, recursively create a dir
            $this->makeDir($path, $foldersCount, $deepLevel);
        }
        // then make some file inside dir
        $this->makeFilesIn($path);
    }

    protected function makeDir($path, $foldersCount, $deepLevel)
    {
        if (file_exists($path)) {
            // delete folder and all inside it
            $files = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS),
                RecursiveIteratorIterator::CHILD_FIRST
            );
            foreach ($files as $fileinfo) {
                $todo = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
                $todo($fileinfo->getRealPath());
            }
            rmdir($path);
            // throw new FolderExistsException("folder {$path} already exists");
        } else {
            // if not, create one
            if ( ! ($newDir = mkdir($path, 0777, true))) {
                throw new Exception("Can`t mkdir()");
            }
        }
        for ($i = 0; $i < $foldersCount; $i++) { 
            $newFolderName = substr(md5(mt_rand()), 0, 20);
            $newPath = $path . DIRECTORY_SEPARATOR . $newFolderName;
            try {
                $this->generateRecursive($newPath, mt_rand(1, $this->dirMaxCount), $deepLevel - 1);
            } catch (Exception $e) {
                continue;
            }
        }
    }

    protected function makeFilesIn($path)
    {
        for ($i = 0; $i < mt_rand(1, $this->fileMaxCount); $i++) { 
            try {
                $this->makeFile($path);
            } catch (Exception $e) {
                continue;
            }
        }
    }

    protected function makeFile($path)
    {
        $isMainExtension = mt_rand(0, 1);
        if ($isMainExtension) {
            $extension = $this->mainExtension;
        } else {
            $extension = $this->extensions[mt_rand(0, count($this->extensions) - 1)];
        }
        $newFileName = substr(md5(mt_rand()), 0, 10) . '.' . $extension;
        $newFilePath = $path . DIRECTORY_SEPARATOR . $newFileName;
        $fp = fopen($newFilePath, 'w');
        if ( ! $fp) {
            throw new Exception("Can`t create file in folder " . $path);
        }
        $this->makeLines($fp, $isMainExtension);
        fclose($fp);
    }

    protected function makeLines($fp, $needContent)
    {
        if ($needContent) {
            fwrite($fp, 'date,geo,zone,impressions,revenue' . PHP_EOL);
            for ($i = 0; $i < mt_rand(1, $this->fileMaxLines); $i++) { 
                $isValidLine = mt_rand(0, 1);
                if ($isValidLine) {
                    $date = '2018-' . mt_rand(1, 12) . '-' . mt_rand(1, 28);
                    $geo = $this->geoCodes[mt_rand(0, count($this->geoCodes) - 1)];
                    $zone = mt_rand(1111111, 9999999);
                    $impressions = mt_rand(1, 100);
                    $revenue = ((int)(mt_rand() / mt_getrandmax() * 100)) / 100;
                    fwrite($fp, "$date,$geo,$zone,$impressions,$revenue" . PHP_EOL);
                } else {
                    fwrite($fp, 'invalid,line,' . substr(md5(mt_rand()), 0, 5) . PHP_EOL);
                }
            }
        } else {
            fwrite($fp, 'some garbage content');
        }
    }
}