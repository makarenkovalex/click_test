<?php
namespace Makarenkov\ClickTest;

use Dotenv\Dotenv;

class Bootstrapper
{
    public static function load()
    {
        $dotenv = new Dotenv(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..');
        $dotenv->load();
        $dotenv->required(['DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASSWORD']);
        $dotenv->required([
            'GENERATOR_DIR_MAX_DEEP', 
            'GENERATOR_DIR_MAX_COUNT', 
            'GENERATOR_FILE_MAX_LINES', 
            'GENERATOR_FILE_MAX_COUNT'
        ]);
    }
}