<?php
namespace Makarenkov\ClickTest;

use PDO;

class Db
{
    protected $connection;

    public function __construct()
    {
        $host = getenv('DB_HOST');
        $port = getenv('DB_PORT');
        $name = getenv('DB_NAME');
        $user = getenv('DB_USER');
        $password = getenv('DB_PASSWORD');
        $this->connection = new PDO("pgsql:host=$host;port=$port;dbname=$name;user=$user;password=$password");
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public static function make()
    {
        return (new self)->getConnection();
    }

    public function getConnection()
    {
        return $this->connection;
    }
}