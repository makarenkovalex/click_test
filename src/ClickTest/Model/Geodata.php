<?php
namespace Makarenkov\ClickTest\Model;

use Makarenkov\ClickTest\Db;

class Geodata
{
    protected $db;

    public function __construct()
    {
        $this->db = Db::make();
    }

    public function find($date, $geo, $zone)
    {
        // echo var_dump($date) . PHP_EOL;
        $stmt = $this->db->prepare("SELECT * FROM geodata WHERE date = :date AND geo = :geo AND zone = :zone");
        $stmt->bindValue(':date', $date);
        $stmt->bindValue(':geo', $geo);
        $stmt->bindValue(':zone', $zone);
        $stmt->execute();
        return $stmt->fetchall();
    }

    public function insert($date, $geo, $zone, $impressions, $revenue)
    {
        $stmt = $this->db->prepare("INSERT INTO geodata VALUES(:date, :geo, :zone, :impressions, :revenue)");
        $stmt->bindValue(':date', $date);
        $stmt->bindValue(':geo', $geo);
        $stmt->bindValue(':zone', $zone);
        $stmt->bindValue(':impressions', $impressions);
        $stmt->bindValue(':revenue', $revenue);
        $stmt->execute();
    }

    public function update($date, $geo, $zone, $impressions, $revenue)
    {
        $stmt = $this->db->prepare("
            UPDATE geodata 
            SET 
                impressions = :impressions,
                revenue =  :revenue 
            WHERE date = :date
            AND geo = :geo 
            AND zone = :zone
        ");
        $stmt->bindValue(':date', $date);
        $stmt->bindValue(':geo', $geo);
        $stmt->bindValue(':zone', $zone);
        $stmt->bindValue(':impressions', $impressions);
        $stmt->bindValue(':revenue', $revenue);
        $stmt->execute();
    }
}