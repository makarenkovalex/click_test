<?php
namespace Makarenkov\ClickTest\Facade;

use Makarenkov\ClickTest\Parser\Parser as RealParser;

class Parser
{
    public static function make()
    {
        return new RealParser;
    }
}