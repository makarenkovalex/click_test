<?php
namespace Makarenkov\ClickTest\Facade;

use Makarenkov\ClickTest\Generator\Generator as RealGenerator;

class Generator
{
    public static function make($extension)
    {
        return new RealGenerator($extension);
    }
}